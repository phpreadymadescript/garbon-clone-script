# garbon-clone-script
<div dir="ltr" style="text-align: left;" trbidi="on">
<h5 style="background-color: white; box-sizing: border-box; color: #222222; font-family: Poppins; font-size: 14px; font-weight: 400; line-height: 18px; margin-bottom: 10px; margin-top: 0px;">
Quick overview:</h5>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
The site features coupons for over 500 online retail brands, across 14 categories. These include all the major retail sites right from Flipkart, eBay, HomeShop18, Indiatimes, Myntra, Pepperfry, Yebhi, to name a few. Looking for coupons on the site is pretty straightforward as it has been divided into several broad categories. You can browse through ‘Top Coupon’, ‘New Coupons’, ‘Top Deals’ and ‘Expiring Coupons’. Alternatively, if you are looking for coupons for a particular product, then you can browse for coupons based on product like cameras, tablets, books, flights, mobile, hotel, furniture etc. You can also browse based on categories.</div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<br /></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;">UNIQUE FEATURES:&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Customer Support&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> SEO Friendly Versions</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Speed optimized Software&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Stand Your Business Brand</span><span style="font-size: 12.996px;">&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Easy script installation&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> User Friendly Scripts&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Dedicated Support Team&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Easy Customization General Features:&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Support different account views&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Information broad casting&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Easy Work flow of buying a deal&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Best Backend features Front-end:&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Login with google/facebook/yahoo&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Profile  Multi-level category.&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Unlimited deal address, location, Contacts, redeem timing, deal nearest address to find out the deal location.&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Unlimited deal image upload.</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Unlimited deal Rate card upload.&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Check Pending, Active, Rejected, Expired Deals.&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Reactivate the expired deals.&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Featured seller deals.&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Check billing history, sell list, and reviews.&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Edit active deals.&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Search through category and subcategory listing pages.&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Search by minimum price and maximum price.&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Search by location and km range.&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> View the rate cards of deals.&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Rate your Deals and add to cart.&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Write a review on deals&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Give feedback to the seller.&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> View the location of the deal through Google map.&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> See the similar deals in your selected city.&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Check discounts  Payment Gateway Enabled Admin:&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Complete CMS system.&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;">&nbsp;</span><span style="font-size: 12.996px;">List of users/sellers.</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> See user details/seller details&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Suspend users, seller&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Generate/edit coupon for discount.&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Check all pending,active,rejected,expired deals,&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Approve pending deals /edited deals&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Option to Edit/Delete deals&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> You can Add/edit/delete category subcategory.&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> You can Add/edit/delete banner ad, feature ads.&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> You can Add/edit/delete payment gateways&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Option to Check Paid/Unpaid/purchased orders.&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Option to Check billing history of seller and user.</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Check reviews.&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Coupon code generation module for admin panel, where we can generate some coupon code to offer additional discount for special occasions.&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Vendor list --- Sort by name, sort by categories, sort by city, sort by expiry date, sort by featured.&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Featured deal is per week basis.</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Admin must be able to see user details (both customer and vendor separately), Details include userid/email, previous orders.&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Option to reset customer password.&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Option to give and subtract some credit from seller account. Other features&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Integrated Google Maps  Show your latest Twitter posts&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Google Analytics integration&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Facebook Connect&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> SSL support&nbsp;</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"> Buy coupon for a friend</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;"><br /></span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span style="font-size: 12.996px;">Check Out Our Product in:</span></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<span id="docs-internal-guid-b1078c11-c68b-4726-1695-5ce818786420"></span><br />
<div dir="ltr" style="line-height: 1.38; margin-bottom: 0pt; margin-top: 0pt;">
<span id="docs-internal-guid-b1078c11-c68b-4726-1695-5ce818786420"><span style="background-color: transparent; color: black; font-size: 11pt; vertical-align: baseline; white-space: pre-wrap;"><a href="https://www.doditsolutions.com/">https://www.doditsolutions.com/</a></span></span></div>
<span id="docs-internal-guid-b1078c11-c68b-4726-1695-5ce818786420">
<div dir="ltr" style="line-height: 1.38; margin-bottom: 0pt; margin-top: 0pt;">
<span style="background-color: transparent; color: black; font-size: 11pt; vertical-align: baseline; white-space: pre-wrap;"><a href="http://scriptstore.in/">http://scriptstore.in/</a></span></div>
<div dir="ltr" style="line-height: 1.38; margin-bottom: 0pt; margin-top: 0pt;">
<span style="background-color: transparent; color: black; font-size: 11pt; vertical-align: baseline; white-space: pre-wrap;"><a href="http://phpreadymadescripts.com/">http://phpreadymadescripts.com/</a></span></div>
</span></div>
</div>
